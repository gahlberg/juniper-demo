.PHONY: all

all: init venv pip
	@echo "Python Setup"

init:
	@echo "Juniper Network Webinar Demo"

venv:
	test -d .venv || virtualenv -p python3 .venv

pip: venv
	.venv/bin/pip3 install --upgrade pip
	.venv/bin/pip3 install -Ur requirements.txt

